<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users','UserController@index')->name('usuarios');
// Route::get('users/create','UserController@create');
// Route::get('users/{id}','UserController@show');

Route::resource('users','UserController');
// Route::get('users/{id}', function($id){
//     return "Detalle del ususario $id";
// });
// Route::get('users/{id}', function($id){
//     return "Detalle del usuario $id";
// })->where('id', '[0-9]+');
// Route::get('users/{id}/{name?}', function($id, $name=null){
//     if($name){
//     return "Detalle del ususario $id. El nombre es $name";
//     }else{
//         return "Detalle del ususario $id. Anónimo";
//     }
// });
