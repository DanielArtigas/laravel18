<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'Lista de usuarios';
        // $users = User::all();
        $users = User::paginate(10);
        // return $users;
        return view('user.index', ['users' => $users]);

        // Nota: En laravel las funciones incorporadas
        // se denominan "helpers", view es una de ellas.
        //busca el fichero /resources/views/user/index.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->name);
        // dd($request->all());

        // validacion
        $request->validate([
            'name' => 'required|max:255|min:5',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255',
        ]);
        //opcion 1
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->remember_token = str_random(10);
        $user->save();

        //opcion 2
        // $user = new User::create($request->all());

        //opcion 3
        // $user = new User();
        // $user->fill($request->all());
        // $user->save();
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //  $user = User::find($id);
        // // return 'Detalles del usuario';
        //  if ($user == null){
        //     abort(403, 'Prohibido!!!');
        //  }
        //$user = User::findOrFail($id);
        return view('user.show', ['user' => $user,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:4',
            'email' => "required|unique:users,email,$id,id|max:255|email",

        ]);

        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();



        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return back();

    }
}
